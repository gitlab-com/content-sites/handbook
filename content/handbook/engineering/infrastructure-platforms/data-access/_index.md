---
title: Data Access Sub Department
---

## Vision

Provide other groups with well-designed interfaces and patterns for efficient
data access that is scalable, reliable, performant, and sustainable for the long
term.

## All Team Members

The following people are permanent members of teams that belong to the Data
Access Sub-department:

### Database Framework

The [Database Framework](/handbook/engineering/infrastructure-platforms/data-access/database-framework/)
team develops solutions for scalability, application performance, data growth and
developer enablement especially where it concerns interactions with the
database.

{{< team-by-manager-slug manager="alexives" >}}

### Database Operations

The [Database Operations](/handbook/engineering/infrastructure-platforms/data-access/database-operations/)
team builds, runis, and owns the entire lifecycle of the PostgreSQL database
engine for GitLab.com.

{{< team-by-manager-slug manager="rmar1" >}}

### Durability

The [Durability](/handbook/engineering/infrastructure-platforms/data-access/durability/)
team is dedicated to safeguarding and securing customer data that is stored by
the GitLab application and set guidelines for data access. We strive to build
and maintain resilient infrastructure and improve the management of Redis,
Sidekiq, and Gitaly.

{{< team-by-manager-slug manager="jarv" >}}

### Gitaly

The [Gitaly](/handbook/engineering/infrastructure-platforms/data-access/gitaly/)
team builds and maintains systems to ensure Git data of GitLab instances, and
_GitLab.com in particular_, is reliable, secure and fast.

{{< team-by-manager-slug manager="jcaigitlab" >}}

### Git

The [Git](/handbook/engineering/infrastructure-platforms/data-access/git/) team
develops Git in accordance with the goals of the community and GitLab, and
integrate it into our products.

{{< team-by-departments "Git Team" >}}
