---
title: Level Up
---

## Leveling Up Level Up

Get ready for **HUGE** changes coming to learning technology for GitLab team members! We are upgrading the technology solution that powers Level Up to better support the pace, demand, complexity, and CULTURE of GitLab. This overhaul will allow team members to engage around learning in a COMPLETELY different way. 

Learning today goes way beyond self-paced content, far passed webinars, leaps above documents; learning is much much more. And GitLab's new "**LevelUp**" (integrated talent EXPERIENCE platform) will show you just how dynamic learning, growth, skills development, and performance can go. 

Consider this page your LevelUp information highway. Over the coming weeks, you'll see updates, announcements, and rollout plans here.

{{< gdoc >}}
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSUJWcpfq-Y_4C7qOIMAT11wJX0n5pAfuKUp9xnQZvuyrESb87_ZISah83q42b4bo-HHOxMnHvNRJSH/embed?start=false&loop=false&delayms=60000"
frameborder="0" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
{{</ gdoc >}}

## What's the difference between LevelUp and GitLab University?

With the launch of this new technology solution, it's important to note that LevelUp and GitLab University our now **two completely separate platforms**, each with a different purpose and audience. GitLab University is used for customer education, whereas LevelUp is designed to support team member learning and professional development.

| | GitLab University | LevelUp |
|---|---|---|
| **Target Audience** | Designed for educating customers and community members | Designed for educating internal team members |
| **Platform** | Runs on ThoughtIndustries Learning System | Runs on Cornerstone Talent Experience Platform |
| **Training Type** | Delivers external-facing product training | Delivers internal training for team members - including product, professional development, and compliance training |

If you'd like to take any courses / certifications that are currently hosted on GitLab University, don't worry, we'll make them accessible via LevelUp.

## Benefits and Features

The new Cornerstone TXP platform offers significant improvements over our current learning management system, specifically designed to support internal employee development and career growth at GitLab. Key features include:

- Internal gigs and side projects to provide hands-on learning opportunities
- Enhanced management tools for training assignment and recommendations
- Advanced user-generated content capabilities
- Seamless integration with our talent management infrastructure, ensuring skills development is reflected in performance data
- Skills library linked to GitLab job architecture 

## Project Team

- Tre Ely - Sr Director Talent Management & Development
- Jamie Allen - Sr Talent Development Program Manager
- Michelle Bencomo - Sr Program Manager, People Leadership Group

## Timeline

Initial launch is planned for the engineering teams in late March 2025, with details on company-wide rollout to follow.
