---
title: "Success Plans"
description: "A success plan is a roadmap that connects a customer's desired business outcomes to GitLab solutions. It is a living document, developed by the CSM."
---

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM-related handbook pages.

---

## Purpose of a success plan

A success plan is the outline and roadmap for achieving customer objectives using GitLab. It is used on an ongoing basis to align on outcomes, action items, and status between the customer and the GitLab team. It is the foundation of a CSM's engagement with their customers.

The success plan is mutually agreed upon by both sides, ensuring the details are reflective of the customer's objectives and GitLab's plan to achieve those objectives and alignment across all involved on those details.

It is a living document, and is reviewed & updated regularly in [cadence calls](/handbook/customer-success/csm/cadence-calls/), [business reviews](/handbook/customer-success/csm/ebr/), [account team meetings](/handbook/customer-success/account-team/#account-team-meetings), and all other situations involving customer journey planning & implementation.

## How a success plan is used

The success plan is part of most interactions with the customer, and as an anchor point for GitLab team planning.

### Customer objectives

Customer objectives are at the heart of a success plan. These are the core results that a customer needs to achieve, and define how we will work with the customer to achieve success. An objective has the following elements:

- A clearly stated goal & outcome
- Measurable success criteria
- A timeline to achieve the objective, with steps & action items outlined in [initiatives](#initiatives)
- Owner(s) and stakeholders

These core elements form the foundation for the rest of the success plan and the initiatives that fall under each objective. Using these details, we can determine the steps needed to achieve the objective and track our progress over time.

### Initiatives

Once an objective has been fully defined, one or more initiatives are created as action plans to achieve the objective. These focus on the "how" to meet the customer outcomes, and enable a division of responsibility for different aspects of the plan.

## Success plan lifecycle and process

### Pre-sales

The success plan starts during the pre-sales phase, driven by the [Solutions Architect](/handbook/customer-success/account-team/#solutions-architect-sa). Throughout the product evaluation process the SA and the [Account Executive](/handbook/customer-success/account-team/#strategic-account-executive-sae--account-executive-ae) define customer objectives, and use these to demonstrate GitLab value aligned to these objectives. The SA documents these [objectives](#customer-objectives) in the [success plan slide deck](#success-plan-components) with all of the requisite details. This information is part of what is used for a [Proof of Value](/handbook/solutions-architects/tools-and-resources/pov/), and ensures that we have a clear understanding of the customer's needed business outcomes.

To understand the full process between Pre-sales and Post-sales, please read about the [mutual customer success plan process](/handbook/solutions-architects/sa-practices/customer-success-plan/).

### Post-sales transition

Once the AE and SA close a customer, the transition to post-sales takes place. The customer goes through [onboarding](/handbook/customer-success/csm/onboarding/), during which the CSM uses the success plan to outline a plan for adoption and aligns with the customer on the details of this plan. This includes a review of the already-listed objectives to ensure accuracy and initiatives for each of those objectives related to enablement, implementation, and other actions needed for the customer to achieve their objectives using GitLab.

Coming out of this phase, there should be full alignment between the GitLab team and the customer stakeholders on the [objectives](#customer-objectives) and the plan to achieve them, including steps for each of the initiatives, ownership of those steps, and commitment from those owners based on defined timelines. Put simply, everyone should be clear on what comes next and who is responsible/accountable for it.

### Enablement and implementation

With the first iteration of the success plan agreed on with the customer, the CSM focuses on putting the plan into action. This has a few key facets:

- Overall management of progress against the plan, including regular reviews with stakeholders and [initiative](#initiatives) owners
- Planning and delivery of enablement sessions (workshops, demos, etc.) to the customer in line with the agreed-upon initiatives
- Recommendations to customer stakeholders on additional areas of opportunity for adoption and value realization based on existing objectives and knowledge of the customer

The most frequent customer engagement point for this is the [cadence call](/handbook/customer-success/csm/cadence-calls/). As the success plan is the hub of our work with the customer, details about initiatives and objectives are incorporated into the cadence call as part of our adoption efforts and overall customer management.

### Business reviews

[Business reviews](/handbook/customer-success/csm/ebr/), similar to [cadence calls](/handbook/customer-success/csm/cadence-calls/), occur on a regular basis and provide the opportunity to review the success plan at a high level and demonstrate progress against the success criteria, as well as discover new objectives for the future.

The success plan and business review should be thought of as mirror images of each other: the information maintained in the success plan feeds the discussion for the business review, and new information attained through the business review meeting is captured in the success plan to add to the roadmap for the customer's success.

## Success Plan Components

A success plan consists of two integrated components: the GitLab-based continuous planning project and the Gainsight success plan. These elements work together to ensure ongoing alignment across all stakeholders and enable measurement and analysis of our efforts.

## GitLab Continuous Planning Project

The success plan is maintained as a living document within a GitLab project, following our continuous planning methodology. This approach provides several key benefits:

- Real-time collaboration and updates through GitLab's native features
- Automated generation of presentation materials through CI/CD pipelines
- Direct integration with daily workflow and project management
- Standardized documentation through epic and issue templates

The structure of the success plan in GitLab organizes objectives as epics and initiatives as issues, with standardized labels and templates ensuring consistent documentation. This makes it easy to track progress, demonstrate measurable results, and maintain up-to-date information that's accessible to all stakeholders.

For ease of discovery and visibility, the GitLab continuous planning project must be linked in the customer's Gainsight success plan using the designated field on the plan info screen (to the right of Approval Status).

## Gainsight Success Plan

Gainsight's success plan capability enables us to analyze and understand patterns across a CSM's book of business and our organization more broadly, helping identify what drives successful use case adoption.

While detailed information about objectives and initiatives lives in the GitLab project, we maintain key objective actions / updated in Gainsight timeline, like customer calls, meeting or similar.

When an objective is identified and documented as an epic in GitLab, it is also synced to Gainsight. Once an objective is achieved, or if it is removed for any reason, it is closed in both GitLab and Gainsight accordingly.

This workflow minimizes duplication while enabling CSMs to maintain visibility into the progress and status of their initiatives across their book of business and track results over time.

## Linking GitLab and Gainsight

GitLab.com serves as the source of truth for Success Plans, with automatic synchronization to Gainsight. This integration enables seamless visibility while reducing manual overhead.

## How It Works

- Success Plans created in GitLab.com (epics and tasks) automatically sync to Gainsight
- Data is transferred via API, creating corresponding Success Plans in Gainsight
- Updates in GitLab.com reflect automatically in Gainsight

## Benefits

- Single source of truth in GitLab.com
- Reduced manual data entry and maintenance
- Consistent Success Plan visibility across platforms
- Eliminates need to maintain plans in multiple locations
