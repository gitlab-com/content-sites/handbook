---
title: "Internal Audit"
description: "Internal Audit function is responsible to assess the effectiveness of risk management, control and governance processes."
---

Internal Audit function is responsible to assess the effectiveness of risk management, control and governance processes. Internal Audit will also provide insight and recommendations that can enhance these processes, particularly relating to effectiveness of operations, reliability of financial management and reporting and Compliance with laws and regulations.

The Internal Audit and SOX function has a career ladder represented below:

| Role                                                | Grade |
|-----------------------------------------------------|-------|
| [Internal Auditor](/job-families/finance/internal-audit/internal-auditor/) <br> [SOX Compliance Analyst](/job-families/finance/internal-audit/internal-auditor/) | 6 |
| [Sr Internal Auditor](/job-families/finance/internal-audit/senior-internal-auditor/) <br> [Sr SOX Compliance Analyst](/job-families/finance/internal-audit/senior-internal-auditor/) | 7 |
| [Internal Audit and SOX Manager](/job-families/finance/internal-audit/internal-audit-and-sox-manager/) <br> [Manager, Internal Audit](/job-families/finance/internal-audit/manager-internal-audit/) <br> [Staff Internal Audit & SOX Compliance Analyst](/job-families/finance/internal-audit/staff-internal-audit-sox-compliance-analyst/) | 8 |
| [Sr Manager, Internal Audit](/job-families/finance/internal-audit/senior-manager-internal-audit/) <br> [Sr Audit Manager, IT and Security](/job-families/finance/internal-audit/senior-audit-manager-it-and-security/) | 9 |
| [Director, Internal Audit](/job-families/finance/internal-audit/director-internal-audit/) <br> [Director, IT Audit](/job-families/finance/internal-audit/director-IT-audit/) | 10 |
| [VP, Internal Audit](/job-families/finance/internal-audit/vp-internal-audit/) | 12 |
